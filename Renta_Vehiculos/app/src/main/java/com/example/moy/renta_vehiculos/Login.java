package com.example.moy.renta_vehiculos;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.moy.renta_vehiculos.General_functions.Common_function;
import com.example.moy.renta_vehiculos.MenuPrincipal.MenuPrincipal;
import com.example.moy.renta_vehiculos.Registrarse.Registrarse;


import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONException;
import org.json.JSONObject;

public class Login extends AppCompatActivity {

    EditText e_usuario;
    EditText e_password;

    String s_usuario="";
    String s_password="";


     //objeto classe asc
    ComprobarCredencialesServer objbajar;

    Dialog customDialog = null;
    public Context contexto = this;
    private static final String PREFRENCES_NAME = "sesionesSharedPreferences";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        e_usuario  = (EditText)findViewById(R.id.Login_editText_usuario);
        e_password = (EditText)findViewById(R.id.Login_editText_password);

    }


    public  void clickBtnMenuPrincipal (View btn){

        s_usuario  = e_usuario.getText().toString().trim();
        s_password = e_password.getText().toString().trim();



        if(s_usuario.equals("")){

            e_usuario.setError("El usuario es requerido");
            e_usuario.requestFocus();

        }else if(s_password.equals("")){

            e_password.setError("La password es requerida");
            e_password.requestFocus();
        }else {

            verificarCargaDatos();
          /*  sesiones_SP( "nombre_usuario", 2);
            llamarMenuPrincipal();*/
        }

       // Toast.makeText(this, "hola", Toast.LENGTH_SHORT).show();

       /* Intent i = new Intent(this, MenuPrincipal.class);

        startActivity(i);
        finish();*/

    }

    /*
	VERIFICO SI EL DISPOSITIVO TIENE DATOS Y CARGA.
	 */
    public void verificarCargaDatos()
    {
        Common_function objCommon_function = new Common_function();
        String mensajeError="";

        if(objCommon_function.isNetworkOnline(this)){
            int cargaBateria = cargaBateria();

            if(calc_bateria_carga(cargaBateria)>15){


                llamarClaseAscincrona();

            }else{
                Toast.makeText(this, "El dispositivo debe estar cargado al menos a un 15%", Toast.LENGTH_LONG).show();
            }
        }else{
            mensajeError="El dispositivo no esta conectado a una red wifi";
            Toast.makeText(this, mensajeError, Toast.LENGTH_LONG).show();
        }

    }

     /*
    verifico si la tableta tiene carga
     */

    public int cargaBateria ()
    {
        try
        {
            IntentFilter batIntentFilter =  new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
            Intent battery =
                    this.registerReceiver(null, batIntentFilter);
            int nivelBateria = battery.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            return nivelBateria;
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),
                    "Error al obtener estado de la batería",
                    Toast.LENGTH_SHORT).show();
            return 0;
        }
    }
    /*
    VERIFICAR SI TIENE AL MENOS UN 15% DE CARGA
     */
    public int calc_bateria_carga(int cargaBateria){
        int porcenteje_bateria=(int)(cargaBateria * 100 / 100);
        return porcenteje_bateria;
    }


    public  void clickRegistrarse (View btn){

        //Intent i= new Intent(this, MapsActivity.class);
        Intent i= new Intent(this, Registrarse.class);
        startActivity(i);

    }


    // llamando clase ascincrona.
    public  void llamarClaseAscincrona (){

        // llamando clase ascincrona.
        objbajar = new ComprobarCredencialesServer(contexto,s_usuario,s_password);
        objbajar.execute();
    }



    // CLASE ASCINCRONA QUE CONSULTA LAS CREDENCIALES EN EL SERVER.
    public class ComprobarCredencialesServer extends AsyncTask<String, Void, String> {

        private ProgressDialog progressDialog;
        public boolean exito=false;
        public boolean serverconexion=true;
        public Common_function objCommonFuction;
        String  url ="";
        Context contexto;
        String nombre_usuario   = "";
        String s_codigo_usuario = "";
        String s_password       = "";
        int perfil_usuario      = 0;
        int respuestaserver     = 0;
        int id_cliente          = 0;

        JSONObject jsonResponse = null;
        public ComprobarCredencialesServer(Context contexto, String s_codigo_usuario, String s_password){
            objCommonFuction=new Common_function();

            this.contexto = contexto;
            this.s_codigo_usuario=s_codigo_usuario;
            this.s_password=s_password;
            this.url=objCommonFuction.url();

        }

        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
                json.put("action", 1);//para el case en el server
                json.put("s_codigo_usuario", s_codigo_usuario);// usuario digitado en el editex
                json.put("s_password", s_password);// password
                //json.put("id", id);//para el case en el server
            } catch (JSONException e) {
                e.printStackTrace();
                //Error creando el objeto que se enviara al server
                //Log.i("Response from server", "pirmer cath");
            }

            HttpClient client = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(client.getParams(), 100000);

            JSONObject jsonResponse = null;
            HttpPost post = new HttpPost(url);
            try {
                StringEntity se = new StringEntity("json="+json.toString());
                post.addHeader("content-type", "application/x-www-form-urlencoded");
                post.setEntity(se);

                HttpResponse response;
                response = client.execute(post); // enviar al servidor  y responde a la misma vez
                String resFromServer = org.apache.http.util.EntityUtils.toString(response.getEntity());

                jsonResponse	= new JSONObject(resFromServer);
                exito			= jsonResponse.getBoolean("exito");
                nombre_usuario  = jsonResponse.getString("nombre_usuario");
                perfil_usuario  = jsonResponse.getInt("codigo_perfil");
                respuestaserver = jsonResponse.getInt("respuestaserver");
                id_cliente      = jsonResponse.getInt("id_cliente");
                //Log.i("response",""+jsonResponse);


            } catch (Exception e) { e.printStackTrace();
                Log.i("Response from server", "" + jsonResponse);
                serverconexion=false;
            }
            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            if(serverconexion==false){
                Toast.makeText(contexto, "  No es posible acceder al servidor", Toast.LENGTH_SHORT).show();
                //errorCompletarCamposLogin("No es posible acceder al servidor");
            }else{
                if (exito == true) {
                    if (respuestaserver == 2) {
                        //Toast.makeText(contexto, "La Password es incorrecta", Toast.LENGTH_SHORT).show();
                        // errorCompletarCamposLogin("La Password es incorrecta");
                        e_password.setError("La password es incorrecta");
                        e_password.requestFocus();
                    } else if(respuestaserver==1) {
                        //Toast.makeText(contexto, "Usuario y pass corectra"+perfil_usuario+nombre_usuario, Toast.LENGTH_SHORT).show();
                        // guardarSesionesSP(nombre_usuario,perfil_usuario);
                        sesiones_SP(nombre_usuario,perfil_usuario);
                        llamarMenuPrincipal();
                    } else {
                        //Toast.makeText(contexto, "El usuario no existe", Toast.LENGTH_SHORT).show();
                        //  errorCompletarCamposLogin("El usuario no existe");
                        e_usuario.setError("El usuario no existe");
                        e_usuario.requestFocus();
                    }
                }

            }

        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(contexto,"","Comprobando credenciales...",true);
        }
    } // FIN DE CLASE  ASCINCRONA!



    /*
     guardo las variables en shared preferences.
   */
    public void sesiones_SP(String nombre_usuario, int perfil_usuario) {
        SharedPreferences preferencias = getSharedPreferences(PREFRENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferencias.edit();

        //	usuario
        editor.putString("nombreusuario_sp", nombre_usuario);  // nombre completo del usuario
        editor.putInt("codigo_perfil", perfil_usuario);			// codigo perfil del usuario
        editor.commit();
    }


    /*
	Llamo la actividad del menú principal.
	 */
    public  void llamarMenuPrincipal(){

        Intent i = new Intent(this, MenuPrincipal.class);
      //  Intent i = new Intent(this, MapsActivity.class);
        startActivity(i);
        finish();
    }

}
