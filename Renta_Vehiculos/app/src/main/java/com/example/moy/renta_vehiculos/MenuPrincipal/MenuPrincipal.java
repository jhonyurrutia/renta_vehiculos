package com.example.moy.renta_vehiculos.MenuPrincipal;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;

import com.example.moy.renta_vehiculos.Buscar.Buscar;
import com.example.moy.renta_vehiculos.Configuracion.ConfiguracionDatos;
import com.example.moy.renta_vehiculos.Historial.Historial;
import com.example.moy.renta_vehiculos.Login;
import com.example.moy.renta_vehiculos.MapsActivity;
import com.example.moy.renta_vehiculos.R;
import com.example.moy.renta_vehiculos.Rentar.Rentar;




public class MenuPrincipal extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        Rentar.OnFragmentInteractionListener,
        Buscar.OnFragmentInteractionListener,
        Historial.OnFragmentInteractionListener,
        ConfiguracionDatos.OnFragmentInteractionListener{

    Dialog customDialog   = null;
    private static final String PREFRENCES_NAME = "sesionesSharedPreferences";
    int codigo_perfil     = 0;
    String nombre_usuario = "";
    TextView t_nombre_usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);

        inicializarControles();
    }


    public  void inicializarControles(){

        sesionSharedPreferences();
        t_nombre_usuario = (TextView)findViewById(R.id.Menu_principal_txt_nombre_usuario);
        t_nombre_usuario.setText(nombre_usuario);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }


    public void sesionSharedPreferences(){

        SharedPreferences preferencias = getSharedPreferences(PREFRENCES_NAME, Context.MODE_PRIVATE);
        nombre_usuario       = preferencias.getString("nombreusuario_sp", "");
        codigo_perfil        = preferencias.getInt("codigo_perfil", 0);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            acercaDe();
            return true;
        }else if (id == R.id.action_desarrollado){

            desarrolladoPor();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void  acercaDe(){



        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(true);
        customDialog.setContentView(R.layout.acerca_de);

        TextView titulo      = (TextView) customDialog.findViewById(R.id.acercaDe_textview_titulo);
        TextView version     = (TextView) customDialog.findViewById(R.id.acercaDe_textview_version);
        TextView ciclo = (TextView) customDialog.findViewById(R.id.acercaDe_textview_cilo);

        titulo.setText("RenCarsApp");
        version.setText("Versión 1.0");
        ciclo.setText("Ciclo. 02-2016");

        customDialog.show();
    }



    public void  desarrolladoPor(){



        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(true);
        customDialog.setContentView(R.layout.lista_alumnos);
        Adaptador adapter= null;
        String [] nombres;
        String [] apellidos={""};
        int[] imagenesAlumnos = {//aca le asiganamos la imagenes que tendra nustro listview

                R.drawable.moy,
                R.drawable.alex,
                R.drawable.lore,
                R.drawable.gerson
        };


        nombres        = getResources().getStringArray(R.array.nombres);
        apellidos      = getResources().getStringArray(R.array.apellidos);
        ListView lista = (ListView) customDialog.findViewById(R.id.listviewalumnos);
        adapter        = new Adaptador(this, nombres, imagenesAlumnos, apellidos);
        lista.setAdapter(adapter);

        customDialog.show();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        android.support.v4.app.Fragment f=null;
        boolean fragmentSeleccionado=false;

        if (id == R.id.nav_camera) {
            // Handle the camera action
            Intent i= new Intent(this, MapsActivity.class);
            startActivity(i);
            finish();

        } else if (id == R.id.nav_gallery) {

            f=new Buscar();
            fragmentSeleccionado=true;

        } else if (id == R.id.nav_slideshow) {

            f=new Rentar();
            fragmentSeleccionado=true;

        } else if (id == R.id.nav_manage) {

            f=new Historial();
            fragmentSeleccionado=true;

        } else if (id == R.id.nav_home) {

            Intent i= new Intent(this, MenuPrincipal.class);
            startActivity(i);
            finish();

        } else  if (id == R.id.nav_share) {

            f=new ConfiguracionDatos();
            fragmentSeleccionado=true;

        } else if (id == R.id.nav_send) {
            Intent i= new Intent(this, Login.class);
            startActivity(i);
            finish();
        }

        if(fragmentSeleccionado)
        {
            getSupportFragmentManager().beginTransaction().replace(R.id.contenedor_principal,f).commit();
            item.setChecked(true);
            getSupportActionBar().setTitle(item.getTitle());
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;


    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
