package com.example.moy.renta_vehiculos.MenuPrincipal;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.moy.renta_vehiculos.R;

/**
 * Created by capacitacion on 08-17-16.
 */
public class Adaptador extends BaseAdapter {

    public Context context;
    public String [] nombres ;
    public String [] apellidos;
    public int    [] imagenes ;
    View fila;



    public Adaptador(Context contexto, String[] nombres, int[] imagen, String[] apellidos){

        //super(contexto, R.layout.fila,descripcion);

        this.context     = contexto;
        this.nombres     = nombres;
        this.imagenes    = imagen;
        this.apellidos   = apellidos;

    }

    @Override
    public int getCount() {
        return apellidos.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    public  View getView (int position, View v, ViewGroup parent){


          LayoutInflater inflar  = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        fila                   = inflar.inflate(R.layout.alumnos,parent, false);
        TextView tvEquipo      = (TextView)fila.findViewById(R.id.tex_nombre);
        TextView tvDescripcion = (TextView)fila.findViewById(R.id.txtApellidos);
        ImageView img          = (ImageView)fila.findViewById(R.id.imgescudo);


        tvEquipo.setText(nombres[position]);
        img.setImageResource(imagenes[position]);
        tvDescripcion.setText(apellidos[position]);
        return  fila;
    }
}
