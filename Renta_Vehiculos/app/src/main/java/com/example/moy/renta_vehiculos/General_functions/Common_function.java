/*------------------------------------------------------
				 06/11/14
				 Ministerio de Salud
				 Samuel Menjivar Nieto
				 Esta clase contiene metodos comunes
 -------------------------------------------------------*/
package com.example.moy.renta_vehiculos.General_functions;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.format.Time;
import android.widget.Toast;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Common_function {
	public String horaNow_class="";



	//regresa una fecha como arreglo a partir de un String de la forma tipo aaaa-mm-dd
		public String[]  getFechaArray(String fechaNac) {
		    String[] fechaArray = fechaNac.split("-");
			return fechaArray;
		}

	
  //averiguamos si tenemos conexion wifi
	public static boolean isNetworkOnline(Context context) {
		//Bandera que nos indica si hay conectividad o no 
	    boolean status=false;
	    try{
	        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	        NetworkInfo netInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
	        if (netInfo != null && netInfo.getState()== NetworkInfo.State.CONNECTED) {
	            status= true;
	        }
	    }catch(Exception e){
	        e.printStackTrace();  
	        return false;
	    }
	    return status;
	    }

	public String getHoraNow(){
        Time today = new Time(Time.getCurrentTimezone());
		today.setToNow();
		String horaNow=today.format("%H:%M:%S");
		String mydate = java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());

		//Log.i("mydate",mydate);
		//Log.i("horaNow",horaNow);
		this.horaNow_class=horaNow;
		return horaNow;
	}
	public String getFechaActual(){
		String fecha_actual="";
		Calendar fecha = new GregorianCalendar();
		int anio = fecha.get(Calendar.YEAR);
		int mes = fecha.get(Calendar.MONTH) + 1;// 0 Enero, 11 Diciembre
		int dia = fecha.get(Calendar.DAY_OF_MONTH);
		fecha_actual=anio+"-"+mes+"-"+dia;
		return fecha_actual;
	}

   public String url(){

	   //String  url = "http://192.168.5.136/ServerCompras/Principal.php";
	   //String url = "http://192.168.5.201/ServerCompras/Principal.php";  //MINSAL
	   String url = "http://192.168.6.146/ServerRentarAuto/Principal.php";  // MINSAL
        //
	   return  url;
   }






}