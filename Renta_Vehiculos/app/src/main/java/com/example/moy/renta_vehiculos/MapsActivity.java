package com.example.moy.renta_vehiculos;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.moy.renta_vehiculos.Buscar.Buscar;
import com.example.moy.renta_vehiculos.MenuPrincipal.MenuPrincipal;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,GoogleMap.OnInfoWindowClickListener
        , Buscar.OnFragmentInteractionListener{

    private GoogleMap mMap;
    private Marker markerSanMiguel;
    private Marker markerZaragoza;
    Dialog customDialog=null;
    Context contexto;
    double longitud =0.0;

    Spinner sp_tipo_mapa;
    int i_tipo_mapa=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        sp_tipo_mapa = (Spinner)findViewById(R.id.Mapa_spinner_tipomapa);
        cargarSpinnerTipoMapa();

        // OBTNER ID SPINNER
        sp_tipo_mapa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                i_tipo_mapa  = pos;

                setiarTipoMapa(i_tipo_mapa);
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    public void  cargarSpinnerTipoMapa(){

        ArrayAdapter spZonaAdapter = ArrayAdapter.createFromResource(this,R.array.tipos_mapas,android.R.layout.simple_list_item_1);
        spZonaAdapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        sp_tipo_mapa.setAdapter(spZonaAdapter);
    }

    public  void setiarTipoMapa(int i_tipo_mapa){

        //Toast.makeText(this, ""+i_tipo_mapa, Toast.LENGTH_SHORT).show();

        if(i_tipo_mapa==1){
            mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        }
        else if(i_tipo_mapa==2){
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        }
        else if(i_tipo_mapa==3){
            mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        }
        else if(i_tipo_mapa==4){

            mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
       /* LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
       */

      /*  LatLng colombia = new LatLng(13.4785076, -88.2391305);
        markerColombia = googleMap.addMarker(new MarkerOptions()
                .position(colombia)
                .title("Colombia")
        );*/



        colocarPuntos(13.4785076, -88.2391305, "San Miguel");
        colocarPuntos(13.6914684,-89.2849075, "Zaragoza");
        googleMap.setOnInfoWindowClickListener(this);


    }


    public void colocarPuntos( double longitud, double latitud, String titulo){
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);


        if(longitud==13.4785076){

            LatLng posision = new LatLng(longitud, latitud);
            markerSanMiguel = mMap.addMarker(
                    new MarkerOptions()
                            .position(posision)
                            .title(titulo)
            );
        }

        if(longitud==13.6914684){

            LatLng posision = new LatLng(longitud, latitud);
            markerZaragoza = mMap.addMarker(
                    new MarkerOptions()
                            .position(posision)
                            .title(titulo)
            );

            mMap.moveCamera(CameraUpdateFactory.newLatLng(posision));
        }


        mMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);


        //googleMap.setOnInfoWindowClickListener(this);
    }

    public void onInfoWindowClick(Marker marker) {

       /* if (marker.equals(markerColombia)) {

            ArgentinaDialogFragment.newInstance(marker.getTitle(),
                    getString(R.string.argentina_full_snippet))
                    .show(getSupportFragmentManager(), null);
        }*/

        if (marker.equals(markerSanMiguel)) {

            alertaConfirmacion("San Miguel","31-3254-12","San Miguel, colonia extraña ");

            //Toast.makeText(this, "san miguel", Toast.LENGTH_SHORT).show();

        }

        if (marker.equals(markerZaragoza)) {

            alertaConfirmacion("Zaragoza","23-1410-47","La libertad,Zaragoza polinono 5");

            //Toast.makeText(this, "Zaragoza", Toast.LENGTH_SHORT).show();

        }

    }


    public void mostrarInformacion(){

    }


    public void alertaConfirmacion(String s_nombre_sucursal,String s_telefono, String s_direccion)
    {

        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customDialog.setCancelable(false);
        customDialog.setContentView(R.layout.informacion_sucursal);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloalerta);


             titulo.setText("Información Sucursal");

        TextView nombre_sucursal = (TextView) customDialog.findViewById(R.id.textview_mensajealerta);
        TextView telefono        = (TextView) customDialog.findViewById(R.id.telefono_sucursal);
        TextView direccion       = (TextView) customDialog.findViewById(R.id.direccion_sucursal);

        nombre_sucursal.setText("Sucursal "+s_nombre_sucursal);
        telefono.setText("Telefono: "+s_telefono);
        direccion.setText("Direción: "+s_direccion);


        /*((Button) customDialog.findViewById(R.id.btn_cancelarAlerta)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
               customDialog.dismiss();


            }
        });*/


        ((Button) customDialog.findViewById(R.id.btn_rentar_Alerta)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
               /* setiarCampos();*/

                //irBuscarSucursal();
            }
        });


        /*
        Intent i= new Intent(this, Buscar.class);
            startActivity(i);

            f= new Buscar();
            fragmentSeleccionado=true;
         */
        customDialog.show();
    }

    public void onBackPressed() {

        Intent i= new Intent(this, MenuPrincipal.class);
        startActivity(i);
        finish();
    }


    public  void irBuscarSucursal(){

        /*Intent i= new Intent(this, Buscar.class);
        startActivity(i);*/

        boolean fragmentSeleccionado=false;
        android.support.v4.app.Fragment f=null;

        f= new Buscar();
        fragmentSeleccionado=true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

}
