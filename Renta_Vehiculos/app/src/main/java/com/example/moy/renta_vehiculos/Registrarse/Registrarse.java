package com.example.moy.renta_vehiculos.Registrarse;

import android.app.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.BatteryManager;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.moy.renta_vehiculos.General_functions.Common_function;
import com.example.moy.renta_vehiculos.Login;
import com.example.moy.renta_vehiculos.MenuPrincipal.MenuPrincipal;
import com.example.moy.renta_vehiculos.R;


import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class Registrarse extends Activity {

    EditText e_nombres;
    EditText e_apellidos;
    EditText e_direccion;
    EditText e_telefono;
    EditText e_nombre_usuario;
    EditText e_password;
    EditText e_confirmar_password;
    EditText e_correo;

    String s_nombres            = "";
    String s_apellidos          = "";
    String s_direccion          = "";
    String s_telefono           = "";
    String s_nombre_usuario     = "";
    String s_password           = "";
    String s_confirmar_password = "";
    String s_correo             = "";


    public Context contexto = this;

    //objeto classe asc
    InsertCliente objCliente;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrarse);


        inicializarControles();
    }

    public  void inicializarControles(){

        e_nombres            = (EditText) findViewById(R.id.Registrarse_editText_nombres);
        e_apellidos          = (EditText) findViewById(R.id.Registrarse_editText_apellidos);
        e_direccion          = (EditText) findViewById(R.id.Registrarse_editText_dirrecion);
        e_telefono           = (EditText) findViewById(R.id.Registrarse_editText_telefono);
        e_nombre_usuario     = (EditText) findViewById(R.id.Registrarse_editText_nombre_usurio);
        e_password           = (EditText) findViewById(R.id.Registrarse_editText_password);
        e_confirmar_password = (EditText) findViewById(R.id.Registrarse_editText_confirme_password);
        e_correo             = (EditText) findViewById(R.id.Registrarse_editText_correo);

    }

    public  void clickGuardarRegistrarse(View btnRegis){

        int respuesta = validarCampos();

        if(respuesta==0){
            if(s_password.equals(s_confirmar_password)){
                verificarCargaDatos();
            }else {
                e_confirmar_password.setError("Las contraseñas no son iguales");
                e_confirmar_password.requestFocus();
            }
        }
    }

    public int validarCampos(){
        int respuesta         = 0;
         s_nombres            = e_nombres.getText().toString().trim();
         s_apellidos          = e_apellidos.getText().toString().trim();
         s_direccion          = e_direccion.getText().toString().trim();
         s_telefono           = e_telefono.getText().toString().trim();
         s_nombre_usuario     = e_nombre_usuario.getText().toString().trim();
         s_password           = e_password.getText().toString().trim();
         s_confirmar_password = e_confirmar_password.getText().toString().trim();
         s_correo             = e_correo.getText().toString().trim();


        if(s_nombres.equals("")){

            e_nombres.setError("Los nombres son requerido");
            e_nombres.requestFocus();
            respuesta=1;

        } else  if(s_apellidos.equals("")){

            e_apellidos.setError("Los apellidos son requerido");
            e_apellidos.requestFocus();
            respuesta=1;

        }else  if(s_direccion.equals("")){

            e_direccion.setError("La dirección es requerida");
            e_direccion.requestFocus();
            respuesta=1;

        }else if(s_telefono.equals("")){

            e_telefono.setError("El telefono es requerido");
            e_telefono.requestFocus();
            respuesta=1;

        }

        else  if(s_correo.equals("")){

            e_correo.setError("El correo es requerido");
            e_correo.requestFocus();
            respuesta=1;

        }

        else if(s_nombre_usuario.equals("")){

            e_nombre_usuario.setError("El nombre de usuario es requerido");
            e_nombre_usuario.requestFocus();
            respuesta=1;

        }else if(s_password.equals("")){

            e_password.setError("La Password es requerido");
            e_password.requestFocus();
            respuesta=1;

        }else if(s_confirmar_password.equals("")) {

            e_confirmar_password.setError("La confirmación es requerido");
            e_confirmar_password.requestFocus();
            respuesta = 1;

        }else{
            respuesta=0;
        }



        return respuesta;
    }


    public void  setiarCampos(){

        s_nombres            = "";
        s_apellidos          = "";
        s_direccion          = "";
        s_telefono           = "";
        s_nombre_usuario     = "";
        s_password           = "";
        s_confirmar_password = "";
        s_correo             = "";

         e_nombres.setText("");
        e_apellidos.setText("");
        e_direccion.setText("");
        e_telefono.setText("");
        e_nombre_usuario.setText("");
        e_password.setText("");
        e_confirmar_password.setText("");
        e_correo.setText("");
    }

    /*
	VERIFICO SI EL DISPOSITIVO TIENE DATOS Y CARGA.
	 */
    public void verificarCargaDatos()
    {
        Common_function objCommon_function = new Common_function();
        String mensajeError="";

        if(objCommon_function.isNetworkOnline(this)){
            int cargaBateria = cargaBateria();

            if(calc_bateria_carga(cargaBateria)>15){

                llamarClaseAscincrona();


            }else{
                Toast.makeText(this, "El dispositivo debe estar cargado al menos a un 15%", Toast.LENGTH_LONG).show();
            }
        }else{
            mensajeError="El dispositivo no esta conectado a una red wifi";
            Toast.makeText(this, mensajeError, Toast.LENGTH_LONG).show();
        }

    }


     /*
    verifico si la tableta tiene carga
     */

    public int cargaBateria ()
    {
        try
        {
            IntentFilter batIntentFilter =  new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
            Intent battery =
                    this.registerReceiver(null, batIntentFilter);
            int nivelBateria = battery.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            return nivelBateria;
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(),
                    "Error al obtener estado de la batería",
                    Toast.LENGTH_SHORT).show();
            return 0;
        }
    }
    /*
    VERIFICAR SI TIENE AL MENOS UN 15% DE CARGA
     */
    public int calc_bateria_carga(int cargaBateria){
        int porcenteje_bateria=(int)(cargaBateria * 100 / 100);
        return porcenteje_bateria;
    }

    // llamando clase ascincrona.
    public  void llamarClaseAscincrona (){

        objCliente = new InsertCliente(contexto, s_nombres,  s_apellidos,  s_direccion,  s_telefono,  s_nombre_usuario,  s_password,  s_correo);
        objCliente.execute();

    }



    // CLASE ASCINCRONA QUE INSERTA UN CLIENTE
    public class InsertCliente extends AsyncTask<String, Void, String> {

        private ProgressDialog progressDialog;
        public boolean exito=false;
        public boolean serverconexion=true;
        public Common_function objCommonFuction;
        String  url ="";
        Context contexto;
        String s_nombres        = "";
        String s_apellidos      = "";
        String s_direccion      = "";
        String s_telefono       = "";
        String s_nombre_usuario = "";
        String s_password       = "";
        String  s_correo        = "";

        JSONObject jsonResponse = null;
        public InsertCliente(Context contexto, String s_nombres, String s_apellidos, String s_direccion, String s_telefono, String s_nombre_usuario, String s_password, String s_correo){
            objCommonFuction=new Common_function();

            this.contexto            = contexto;
            this.s_nombres           = s_nombres;
            this.s_apellidos          = s_apellidos;
            this.s_direccion          = s_direccion;
            this.s_telefono           = s_telefono;
            this.s_nombre_usuario     = s_nombre_usuario;
            this.s_password           = s_password;
            this .s_correo            = s_correo;
            this.url=objCommonFuction.url();

        }

        protected String doInBackground(String... params) {
            JSONObject json = new JSONObject();
            try {
                json.put("action", 2);//para realizar acciones a la tabla cliente
                json.put("actionTabla", 1);// quiere decir que va insertar en clientes
                json.put("nombres_clientes", s_nombres);
                json.put("apellidos_cliente", s_apellidos);
                json.put("direccion_cliente", s_direccion);
                json.put("telefono_cliente", s_telefono);
                json.put("correo_cliente", s_correo);
                json.put("codigo_usuario", s_nombre_usuario);
                json.put("clave_usuario", s_password);


                //json.put("id", id);//para el case en el server
            } catch (JSONException e) {
                e.printStackTrace();
                //Error creando el objeto que se enviara al server
                //Log.i("Response from server", "pirmer cath");
            }

            HttpClient client = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(client.getParams(), 100000);

            JSONObject jsonResponse = null;
            HttpPost post = new HttpPost(url);
            try {
                StringEntity se = new StringEntity("json="+json.toString());
                post.addHeader("content-type", "application/x-www-form-urlencoded");
                post.setEntity(se);

                HttpResponse response;
                response = client.execute(post); // enviar al servidor  y responde a la misma vez
                String resFromServer = org.apache.http.util.EntityUtils.toString(response.getEntity());

                jsonResponse	= new JSONObject(resFromServer);
                exito			= jsonResponse.getBoolean("exito");



            } catch (Exception e) { e.printStackTrace();
                Log.i("Response from server", "" + jsonResponse);
                serverconexion=false;
            }
            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            if(serverconexion==false){
                Toast.makeText(contexto, "  No es posible acceder al servidor", Toast.LENGTH_SHORT).show();

            }else{
                if (exito == true) {

                    setiarCampos();
                    Toast.makeText(contexto, "Cliente insertado correctamente", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(contexto, "Fallo al insertar cliente", Toast.LENGTH_SHORT).show();
                }

            }

        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(contexto,"","Insertando cliente...",true);
        }
    } // FIN DE CLASE  ASCINCRONA!



    public  void retornarInicio (View retornar){


        Intent i = new Intent(this, Login.class);
        startActivity(i);
        finish();
    }

}
